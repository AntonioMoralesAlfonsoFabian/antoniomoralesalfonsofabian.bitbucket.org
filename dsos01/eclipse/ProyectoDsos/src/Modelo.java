import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;
public class Modelo extends AbstractTableModel {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String[] columnNames;
    private ArrayList<Object[]> data;
    private Class<?> columClas[];
    public Modelo(String []colum,Class<?> colClas[]){
    	columnNames=colum;
    	columClas=colClas;
    	data=new ArrayList<Object[]>();
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return columClas[columnIndex];
    }
    public int getColumnCount() {
        return columnNames.length;
    }
    public int getRowCount() {
        return data.size();
    }
    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }
    @Override
    public Object getValueAt(int row, int col) {
        return data.get(row)[col];
    }
    public void addRow(Object[] x) {
    	data.add(x);
    	fireTableDataChanged();
    }
    public void removeRow(String x,int y) {
    	for(int i=0;i<data.size();i++) {
    		if(data.get(i)[y].equals(x)) {
    			data.remove(i);
    		}
    	}
    	for(int i=0;i<data.size();i++) {
    		data.get(i)[0]=(i+1);
    	}
    	fireTableDataChanged();
    }
}