import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JList;

public class Operaciones extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private int operaciones[];
	private int restricciones[];

	public Operaciones() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 900, 528);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(274, 73, 189, 318);
		contentPane.add(scrollPane);
		
		JList<String>  list2 = new JList<String>();
		scrollPane.setViewportView(list2);
		list2.setListData(new String[] {"SUMA","RESTA","MULTIPICACION","DIVICION","PARTE ENTERA","PARTE DECIMAL","REDONDEAR"});
		operaciones=new int[] {1,1,1,1,0,0,0};
		System.out.println(list2.getSelectedIndex());
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(614, 73, 240, 318);
		contentPane.add(scrollPane_1);
		
		Lista lista = new Lista();
		scrollPane_1.setViewportView(lista);
		
		JLabel lblOperacionesDisponibles = new JLabel("Operaciones Disponibles:");
		lblOperacionesDisponibles.setBounds(274, 46, 189, 15);
		contentPane.add(lblOperacionesDisponibles);
		
		JLabel lblOperacionesSeleccionadas = new JLabel("Operaciones Seleccionadas:");
		lblOperacionesSeleccionadas.setBounds(626, 46, 213, 15);
		contentPane.add(lblOperacionesSeleccionadas);
		
		JButton btnAgregar = new JButton("Agregar >>");
		btnAgregar.setBounds(475, 92, 117, 25);
		contentPane.add(btnAgregar);
		
		JButton button = new JButton("<< Quitar");
		button.setBounds(475, 140, 117, 25);
		contentPane.add(button);
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				new Resultados().setVisible(true);
			}
		});
		btnAceptar.setBounds(475, 432, 117, 25);
		contentPane.add(btnAceptar);
		
		JLabel lblSelecciones = new JLabel("Seleccione que Restricciones y Operaciones se aplicara al archivo:");
		lblSelecciones.setBounds(187, 19, 514, 15);
		contentPane.add(lblSelecciones);
		
		JButton btnSubir = new JButton("Subir >>");
		btnSubir.setBounds(475, 191, 117, 25);
		contentPane.add(btnSubir);
		
		JButton btnBajar = new JButton("Bajar >>");
		btnBajar.setBounds(475, 228, 117, 25);
		contentPane.add(btnBajar);
		
		JLabel label = new JLabel("Restricciones Disponibles:");
		label.setBounds(34, 46, 189, 15);
		contentPane.add(label);
		
		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(34, 73, 189, 318);
		contentPane.add(scrollPane_2);
		
		JList<String>  list = new JList<String>();
		scrollPane_2.setViewportView(list);
		list.setListData(new String[] {"IMPARES","PARES","POSITIVOS","NEGATIVOS","ENTERO","FLOTANTE","IGUAL A","DISTINTO DE","ESTA EN RANGO","NO ESTA EN RANGO"});
		restricciones=new int[] {0,0,0,0,0,0,1,1,2,2};
		setResizable(false);
		setLocationRelativeTo(null);
		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(list2.getSelectedIndex()!=-1) {
					lista.addFila(list2.getSelectedValue(),operaciones[list2.getSelectedIndex()]);
					System.out.println(list2.getSelectedValue());
				}else if(list.getSelectedIndex()!=-1) {
					lista.addFila(list.getSelectedValue(),restricciones[list.getSelectedIndex()]);
					System.out.println(list.getSelectedValue());
				}
			}
		});
	}
}
