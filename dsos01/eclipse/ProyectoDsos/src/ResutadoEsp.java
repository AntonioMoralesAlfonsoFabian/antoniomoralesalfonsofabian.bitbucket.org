import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.border.BevelBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class ResutadoEsp extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextField txtWwwejemplocom;
	private JTextField txtMilisegundos;
	private JTable table;
	public ResutadoEsp() {
		setBounds(100, 100, 649, 377);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBounds(0, 298, 643, 42);
			contentPanel.add(buttonPane);
			buttonPane.setLayout(null);
			{
				JButton regresarButton = new JButton("Regresar");
				regresarButton.setBounds(258, 12, 115, 25);
				regresarButton.setActionCommand("Cancel");
				buttonPane.add(regresarButton);
			}
		}
		{
			JPanel panel = new JPanel();
			panel.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
			panel.setBounds(0, 0, 643, 66);
			contentPanel.add(panel);
			panel.setLayout(null);
			{
				JLabel lblServidor = new JLabel("Servidor:");
				lblServidor.setBounds(96, 12, 70, 15);
				panel.add(lblServidor);
			}
			{
				txtWwwejemplocom = new JTextField();
				txtWwwejemplocom.setText("www.ejemplo.com");
				txtWwwejemplocom.setEnabled(false);
				txtWwwejemplocom.setBounds(164, 10, 434, 19);
				panel.add(txtWwwejemplocom);
				txtWwwejemplocom.setColumns(10);
			}
			{
				JLabel lblTiempoDeConexion = new JLabel("Tiempo de conexion:");
				lblTiempoDeConexion.setBounds(12, 39, 154, 15);
				panel.add(lblTiempoDeConexion);
			}
			{
				txtMilisegundos = new JTextField();
				txtMilisegundos.setText("100 milisegundos");
				txtMilisegundos.setEnabled(false);
				txtMilisegundos.setColumns(10);
				txtMilisegundos.setBounds(164, 39, 434, 19);
				panel.add(txtMilisegundos);
			}
		}
		{
			Modelo df=new Modelo(new String[]{"Num","Col1","Col2"}, new Class[] {String.class,String.class,String.class});
			DefaultTableCellRenderer tcr = new DefaultTableCellRenderer();
			tcr.setHorizontalAlignment(SwingConstants.CENTER);
			df.addRow(new Object[]{1,20,10});
			JScrollPane scrollPane = new JScrollPane();
			scrollPane.setBounds(0, 78, 643, 219);
			contentPanel.add(scrollPane);
			{
				table = new JTable();
				table.setModel(df);
				table.getColumnModel().getColumn(0).setPreferredWidth(10);
				table.getColumnModel().getColumn(1).setPreferredWidth(200);
				table.getColumnModel().getColumn(2).setPreferredWidth(200);
				table.getColumnModel().getColumn(0).setCellRenderer(tcr);
				table.getColumnModel().getColumn(1).setCellRenderer(tcr);
				table.getColumnModel().getColumn(2).setCellRenderer(tcr);
				scrollPane.setViewportView(table);
			}
		}
		setResizable(false);
		setLocationRelativeTo(null);
	}

}
