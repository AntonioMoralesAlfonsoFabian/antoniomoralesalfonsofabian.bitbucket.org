import java.awt.GridLayout;
import java.util.ArrayList;
import javax.swing.JPanel;

public class Lista extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<Fila> lst;
	private JPanel jp;
	public Lista() {
		lst=new ArrayList<Fila>();
		jp=new JPanel(new GridLayout(0, 1));
		
	}
	public void addFila(String msg,int x) {
		if(x==0) {
			lst.add(new Fila(msg));
		}else {
			lst.add(new Fila(x,msg));
		}
		System.out.println(lst.size());
		removeAll();
		jp.removeAll();
		for(int i=0;i<lst.size();i++) {
			jp.add(lst.get(i));
		}
		this.add(jp);
		repaint();
	}
	public void removeFila(int x) {
		if(x<lst.size()) {
			lst.remove(x);
			removeAll();
			jp.removeAll();
			for(int i=0;i<lst.size();i++) {
				jp.add(lst.get(i));
			}
			this.add(jp);
		}
		repaint();
	}
	public void removeFila() {
		int x[]=getSelected();
		for(int i=0;i<x.length;i++) {
			lst.remove(x[i]);
		}
		removeAll();
		jp.removeAll();
		for(int i=0;i<lst.size();i++) {
			jp.add(lst.get(i));
		}
		this.add(jp);
		repaint();
	}
	public int[] getSelected() {
		int x[],cont=0;
		for(int i=0;i<lst.size();i++) {
			if(lst.get(i).jrb.isSelected()) {
				cont++;
			}
		}
		x=new int[cont];cont=0;
		for(int i=0;i<lst.size();i++) {
			if(lst.get(i).jrb.isSelected()) {
				x[cont]=i;
				cont++;
			}
		}
		return x;
	}
}
