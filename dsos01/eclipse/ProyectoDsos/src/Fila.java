import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class Fila extends JPanel implements KeyListener{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JRadioButton jrb;
	JTextField jtf[];
	private JPanel jp[];
	public Fila(int x,String msg) {
		jrb=new JRadioButton("");
		jtf=new JTextField[x+1];
		if(msg.length()==18)
			jtf[0]=new JTextField(msg);
		else if(msg.length()<18){
			for(int i=msg.length();i<17;i++) {
				msg+=" ";
			}
		}
		jtf[0]=new JTextField(msg);
		jtf[0].setEditable(false);
		for(int i=1;i<x+1;i++) {
			jtf[i]=new JTextField("      ");
			jtf[i].addKeyListener(this);
		}
		this.setLayout(new FlowLayout(1));
		this.add(jrb);
		jp=new JPanel[3];
		jp[0]=new JPanel(new FlowLayout(1));
		jp[1]=new JPanel(new FlowLayout(1));
		jp[2]=new JPanel(new BorderLayout());
		for(int i=1;i<x+1;i++) {
			jp[1].add(jtf[i]);
		}
		jp[0].add(jtf[0]);
		jp[2].add(jp[0],BorderLayout.CENTER);
		jp[2].add(jp[1],BorderLayout.EAST);
		this.add(jp[2]);
	}
	public Fila(String msg) {
		jrb=new JRadioButton("");
		jtf=new JTextField[1];
		if(msg.length()==18)
			jtf[0]=new JTextField(msg);
		else if(msg.length()<18){
			for(int i=msg.length();i<17;i++) {
				msg+=" ";
			}
		}
		jtf[0]=new JTextField(msg);
		jtf[0].setEditable(false);
		this.setLayout(new FlowLayout(1));
		this.add(jrb);
		this.add(jtf[0]);
	}
	public Fila() {
		jrb=new JRadioButton("");
		jtf=new JTextField[1];
		jtf[0]=new JTextField("                   ");
		jtf[0].setEditable(false);
		this.setLayout(new FlowLayout(1));
		this.add(jrb);
		this.add(jtf[0]);
	}
	@Override
	public void keyTyped(KeyEvent e) {
		if(e.getKeyChar()!='.') {
			if(!Character.isDigit(e.getKeyChar()))
				e.consume();
		}
		
	}
	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
}
