import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.border.BevelBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

public class Principal extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField;
	private JTable table;
	private ArrayList<String> url;
	private Modelo df;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Principal frame = new Principal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	public Principal() {
		url = new ArrayList<String>();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 597, 408);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel.setBounds(46, 12, 517, 109);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Ingrese la direccion del servidor:");
		lblNewLabel.setBounds(12, 12, 248, 15);
		panel.add(lblNewLabel);
		
		textField = new JTextField();
		textField.setBounds(12, 39, 493, 19);
		panel.add(textField);
		textField.setColumns(10);
		
		JButton btnAgregar = new JButton("Agregar");
		btnAgregar.setBounds(134, 70, 117, 25);
		panel.add(btnAgregar);
		
		JButton btnEliminar = new JButton("Eliminar");
		btnEliminar.setBounds(278, 70, 117, 25);
		panel.add(btnEliminar);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(46, 141, 517, 183);
		contentPane.add(scrollPane);
		
		df=new Modelo(new String[]{"Num","Servidores"}, new Class[] {Boolean.class,String.class,String.class});
		table = new JTable();
		table.setModel(df);
		table.getColumnModel().getColumn(0).setPreferredWidth(10);
		table.getColumnModel().getColumn(1).setPreferredWidth(400);
		DefaultTableCellRenderer tcr = new DefaultTableCellRenderer();
		tcr.setHorizontalAlignment(SwingConstants.CENTER);
		table.getColumnModel().getColumn(0).setCellRenderer(tcr);
		table.getColumnModel().getColumn(1).setCellRenderer(tcr);
		scrollPane.setViewportView(table);
		
		JButton btnSiguiente = new JButton("Aceptar");
		btnSiguiente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				new Operaciones().setVisible(true);;
			}
		});
		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (textField.getText().length() !=0) 
					if (url.contains(textField.getText())== false)  {
						url.add(textField.getText());
						textField.setText(null);
						System.out.println(url);//prueba de Array
						df.addRow(new Object[]{url.size(),url.get(url.size()-1)});
				
					}
					
			}
		});
		btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (textField.getText().length() !=0) 
					if (url.contains(textField.getText())== true)  {
				url.remove(textField.getText());
				df.removeRow(textField.getText(), 1);
				textField.setText(null);
				System.out.println(url);//prueba de Array
			}}
		});
		btnSiguiente.setBounds(256, 346, 117, 25);
		contentPane.add(btnSiguiente);
		setResizable(false);
		setLocationRelativeTo(null);
	}
}
