//Antonio Morales Alfonso Fabian
var Ajedrez = (function(){
    var control=true;
    var mensaje;
    var data;
        var _mostrar_tablero = function(){
            var filas = 10;
            var columnas = 10;
            var body = document.getElementById("tablero");
            var tabla   = document.createElement("table");
            var tblBody = document.createElement("tbody");
            mensaje = document.getElementById("mensaje");
            for (var i = 1; i < filas-1; i++) {
              var hilera = document.createElement("tr");
              for (var j = 1; j < columnas-1; j++) {
                var celda = document.createElement("td");
                if(i%2 == 0){
                    if(j%2==0){
                        celda.setAttribute("class", "blancas");
                    }
                    else{
                        celda.setAttribute("class", "negras");
                    }
                }else{
                    if(j%2==0){
                        celda.setAttribute("class", "negras");
                    }
                    else{
                        celda.setAttribute("class", "blancas");
                    }
                }
                celda.textContent = " ";
                hilera.appendChild(celda);
              }
              tblBody.appendChild(hilera);
            }
            tabla.appendChild(tblBody);
            tabla.setAttribute('id', 'tabla');
            body.appendChild(tabla);
            var arr_filas = document.getElementsByTagName('tr');
            var peones_negros = arr_filas[1];
            for(var i  = 0; i< peones_negros.children.length; i++){
                peones_negros.children[i].textContent = "\u265f";
            }
            var peones_blancos = arr_filas[6];
            for(var i  = 0; i< peones_blancos.children.length; i++){
                peones_blancos.children[i].textContent = "\u2659";
            }
            var linea_negra = arr_filas[0];
            linea_negra.children[0].textContent = "\u265c";
            linea_negra.children[1].textContent = "\u265e";
            linea_negra.children[2].textContent = "\u265d";
            linea_negra.children[3].textContent = "\u265b";
            linea_negra.children[4].textContent = "\u265a";
            linea_negra.children[5].textContent = "\u265d";
            linea_negra.children[6].textContent = "\u265e";
            linea_negra.children[7].textContent = "\u265c";
            var linea_blanca = arr_filas[7];
            linea_blanca.children[0].textContent = "\u2656";
            linea_blanca.children[1].textContent = "\u2658";
            linea_blanca.children[2].textContent = "\u2657";
            linea_blanca.children[3].textContent = "\u2655";
            linea_blanca.children[4].textContent = "\u2654";
            linea_blanca.children[5].textContent = "\u2657";
            linea_blanca.children[6].textContent = "\u2658";
            linea_blanca.children[7].textContent = "\u2656";
            var num = [8,7,6,5,4,3,2,1];
            var ltr = ['a','b','c','d','e','f','g','h'];
            var filas = document.getElementsByTagName('tr');
            for(var i = 0; i<filas.length; i++){
                for(var j = 0; j< filas[0].children.length; j++){
                    var nombre = ""+ltr[j]+num[i];
                    var celda = filas[i].children[j];
                    celda.setAttribute("name", nombre);
                }
            }
            var cnt=document.createElement("center");
            var inp=document.createElement("input");
            var opc=document.getElementById("opcion");
            inp.setAttribute("type","submit");
            inp.setAttribute("value","actualizarTablero");
            inp.setAttribute("onclick","Ajedrez.actualizarTablero()");
            cnt.appendChild(inp);
            opc.appendChild(cnt);
          }
          var _actualiza_tablero=function(){
            var url = "https://antoniomoralesalfonsofabian.bitbucket.io/Ajedrez/csv/tablero.csv"
            var xhr = new XMLHttpRequest();
		    xhr.open('GET', url, true);
		    xhr.onreadystatechange = function (){
			    if(xhr.readyState === 4){
				    if(xhr.status === 200 || xhr.status == 0){
                        data = (xhr.responseText.split(/\r?\n|\r/));
			        }
			    }else{
                    alert("A ocurrido un error con el servidor");
                    mensaje.textContent = "A ocurrido un error con el servidor";
                }
	    	}
            xhr.send(null);
            //_recargar_tabla();
          }
          var _recargar_tabla=function(){
            var filas = document.getElementsByTagName('tr');  
              for(var i=1;i<8;i++){
                if(!control)
                break;
                var tem = arr_filas[i];
                var dt=data[i].split("|");
                  for(var j=0;j<8;i++){
                      if(!control)
                      break;
                    switch (dt[j]) {
                        case " ":
                          break;
                        case "u265c":
                        tem.children[j].textContent = "\u265c";
                          break;
                        case "u265e":
                        tem.children[j].textContent = "\u265e";
                          break;
                        case "u265d":
                        tem.children[j].textContent = "\u265d";
                          break;
                        case "u265b":
                        tem.children[j].textContent = "\u265b";
                          break;
                        case "u265a":
                        tem.children[j].textContent = "\u265a";
                        break;
                        case "u265f":
                        tem.children[j].textContent = "\u265f";
                          break;
                        case "u2659":
                        tem.children[j].textContent = "\u2659";
                          break;
                        case "u2656":
                        tem.children[j].textContent = "\u2656";
                          break;
                        case "u2658":
                        tem.children[j].textContent = "\u2658";
                          break;
                        case "u2657":
                        tem.children[j].textContent = "\u2657";
                          break;
                        case "u2655":
                        tem.children[j].textContent = "\u2655";
                          break;
                        case "u2654":
                        tem.children[j].textContent = "\u2654";
                        break;
                        default:
                          control=false;
                      }
                  }
              }
              if(!control){
                _mostrar_tablero();
                alert("A ocurrido un error con el servidor");
                mensaje.textContent = "A ocurrido un error con el servidor";
              }
          }
          var _mover_pieza = function(coord){
              var org = coord.de;
              var des = coord.a;
              var celda_org = document.getElementsByName(org)[0];
              var celda_des = document.getElementsByName(des)[0];
              var pieza = celda_org.textContent;
              if(org.charAt(1)<9&&org.charAt(1)>0){
                if(des.charAt(1)<9&&des.charAt(1)>0){
                    if(org!=des){
                        if(" " != pieza){
                            celda_org.textContent = " ";
                            celda_des.textContent = pieza;
                        }else{
                            alert("error esta moviendo la pieza a su misma posicion");
                            mensaje.textContent = "error esta moviendo la pieza a su misma posicion";
                        }
                      }else{
                            alert("error esta moviendo la pieza a su misma posicion");
                            mensaje.textContent = "error esta moviendo la pieza a su misma posicion";
                      }
                  }else{
                    alert("error esta moviendo la pieza a una ubicacion inexistente");
                    mensaje.textContent = "error esta moviendo la pieza a una ubicacion inexistente";
                  }
              }else{
                alert("error la ubicacion de la pieza es inexistente");                      
                mensaje.textContent = "error la ubicacion de la pieza es inexistente";
              }
          }
          return{
              "mostrarTablero": _mostrar_tablero,
              "actualizarTablero":_actualiza_tablero,
              "moverPieza": _mover_pieza   
          }
    })();